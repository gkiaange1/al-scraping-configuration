import json
import requests
import display_handler


def test_frontpage(edition: str, source: str):
    with open(f'{edition}/source.json') as json_file:
        selected_edition = json.load(json_file)
        data = [item for item in selected_edition['frontpage_items'] if item["source"] == source][0]
        data['proxy'] = ""
        url = 'https://gpz5a716s2.execute-api.eu-west-1.amazonaws.com/dev/test_frontpage'

        output = requests.post(url, json=data)
        display_handler.print_in_file(output)
        items = [output.json()["sucess"]["result"]["mainItem"]] + output.json()["sucess"]["result"]["secondaryItems"]
        display_handler.print_html_in_browser(items)


if __name__ == "__main__":
    test_frontpage("GB", "thenational.scot")
