**This project is to create a configuration json file for our news crawler**

Go through the list with the domains included at sites.json file.
For each of them you will need to create 2 configuration object for our crawler to scrape them as described below.

---

## Collection item

This represents the latest news feed for the news website.

### Example:
If we wanted to scrape https://lajmifundit.al/
we would need to create the below for the latest news feed

{
      "edition": "AL",
      "source": "lajmifundit.al",
      "item_type": "collection",
      "category": "category_news",
      "base_url": "https://lajmifundit.al",
      "path": "/category/aktualitet",
      "content_selector": null,
      "item_selector": {
        "css": [
          "article"
        ]
      },
      "action_selector": {
        "css": [
          "a ::attr(href)"
        ]
      },
      "title_selector": {
        "css": [
          "h2 ::text"
        ]
      },
      "image_selector": {
        "css": [
          "[class*='a-media_img']::attr(data-src)"
        ]
      }
    }
#### category

You would need to specify the category of the news, for examplke the site might be a news or sports news site.

#### item_selector

Refers to the tag of the item of news feed

##### css Tag


It is an array of css tags. So if some items have different tags, you can include them all
for example
"css": [
          "a ::attr(href)", "a::attr(style)"//these are just an example
        ]

If you want to work with xpath it would look as below

"x_path": [ "//*[@class=\"listing-title\"]//a/text()", "//*[contains(@class, \"module-simple\")]" ]

#### action_selector

Refers to the url of the article. Same approach as above

#### title_selector

Refers to the title of the article. Same approach as above

#### image_selector

Refers to the image url of the article. Same approach as above


---

## Front page  item

This represents the latest news feed for the news website.

### Example:
If we wanted to scrape https://lajmifundit.al/
we would need to create the below for the latest news feed

{
      "edition": "AL",
      "source": "lajmifundit.al",
      "item_type": "front_page",
      "category": "category_news",
      "base_url": "https://lajmifundit.al",
      "path": "/",
      "main_item": null,
      "secondary_item": {
        "content": {
          "css": [
            "[class*='owl-carousel'] article"
          ]
        },
        "action_selector": {
          "css": [
            "a::attr(href)"
          ]
        },
        "title_selector": {
          "css": [
            "h2 ::text"
          ]
        },
        "image_selector": {
          "css": [
            "img::attr(data-src)"
          ]
        }
      }
    }

#### category

You would need to specify the category of the news, for examplke the site might be a news or sports news site.

The categories we usally support are:
##### category_news

News in general

##### category_sport

Sports in general

##### category_rest 

Interesting interesting stuff that do not match the other categories

##### category_ent

Entertainment/showbiz news

##### category_man

Man related news

##### category_lady

Women related news


##### category_tech

Technology news

##### category_car 

Car related news

##### category_science

Science news

##### category_health

Health news

##### category_defence

Defence

##### category_local_news

Local news p.x. Korce

##### category_cinema 

Movies/cinema/tv

#### main_item

Refers to the tag of the main front page item of the news web site. Has the same format with the secondary_item, if the main item has the same tags as the secondary item then it can be null.

#### secondary_item

Refers to the tag of the secondary front page items of the news web site. needs to provide at least 3 articles.

"content": {
          "css": [
            "[class*='owl-carousel'] article"
          ]
        }

##### css Tag


It is an array of css tags. So if some items have different tags, you can include them all
for example
"css": [
          "a ::attr(href)", "a::attr(style)"//these are just an example
        ]

If you want to work with xpath it would look as below

"x_path": [ "//*[@class=\"listing-title\"]//a/text()", "//*[contains(@class, \"module-simple\")]" ]

#### action_selector

Refers to the url of the article. Same approach as above

#### title_selector

Refers to the title of the article. Same approach as above

#### image_selector

Refers to the image url of the article. Same approach as above

## Testing

You can test if the config object is correct and can scrape the site as expected by calling the below apis
POST
https://gpz5a716s2.execute-api.eu-west-1.amazonaws.com/dev/test_frontpage

sample data

{
      "edition": "AL",
      "source": "lajmifundit.al",
      "item_type": "front_page",
      "category": "category_news",
      "base_url": "https://lajmifundit.al",
      "path": "/",
      "main_item": null,
      "secondary_item": {
        "content": {
          "css": [
            "[class*='owl-carousel'] article"
          ]
        },
        "action_selector": {
          "css": [
            "a::attr(href)"
          ]
        },
        "title_selector": {
          "css": [
            "h2 ::text"
          ]
        },
        "image_selector": {
          "css": [
            "img::attr(data-src)"
          ]
        }
      }
    }


https://gpz5a716s2.execute-api.eu-west-1.amazonaws.com/dev/test_collection
POST
{
      "edition": "AL",
      "source": "lajmifundit.al",
      "item_type": "collection",
      "category": "category_news",
      "base_url": "https://lajmifundit.al",
      "path": "/category/aktualitet",
      "content_selector": null,
      "item_selector": {
        "css": [
          "article"
        ]
      },
      "action_selector": {
        "css": [
          "a ::attr(href)"
        ]
      },
      "title_selector": {
        "css": [
          "h2 ::text"
        ]
      },
      "image_selector": {
        "css": [
          "[class*='a-media_img']::attr(data-src)"
        ]
      }
    }
