import webbrowser
import os
import json

def print_html_in_browser(items):
    row = """<article>
            <div class="post-container">
            <div class="post-thumb"><img src="{IMG}" height="120px" width="auto" alt="" /></div>
            <div class="post-content">
            <h2 class="post-title"><a href="{LINK}" title="">{TITLE}</a></h2>
            </div>
            </article>
            </div>"""
    li = "<li class='review_bottom'>{CONTENT}</li>"

    html = """"
    <html>
    <style>
    .post-container {
        margin: 20px 20px 0 0;
        overflow: auto
        }
        .post-thumb {
            float: left
        }
        .post-thumb img {
            display: block
        }
        .post-content {
            margin-left: 210px
        }
        .post-title {
            font-weight: semibold;
            font-size: 150%
            vertical-align: text-top;
        }
    </style>
    <ul>{CONTENT}</ul></html>"""
    content = ""
    for item in items:
        image = item["image"]
        link = item["articleAction"].split("::")[2]
        title = item['title']

        li_item = li.replace("{CONTENT}", row.replace("{IMG}", image)
                             .replace("{LINK}", link)
                             .replace("{TITLE}", title))
        content = content + li_item
    html = html.replace("{CONTENT}", content)
    f = open('output' + '.html', 'w', encoding='UTF-8')
    f.write(html)
    f.close()
    filename = 'file:///' + os.getcwd() + '/' + 'output' + '.html'
    webbrowser.open_new_tab(filename)

def print_in_file(output):
    f = open('output' + '.json', 'w')
    f.write(json.dumps(json.loads(output.text), ensure_ascii=False, indent=4, sort_keys=False))
    f.close()
    filename = 'file:///' + os.getcwd() + '/' + 'output' + '.json'
    webbrowser.open_new_tab(filename)