import json
import requests
import os
import webbrowser
import display_handler


def test_collection(edition: str, source: str, category: str):
    with open(f'{edition}/source.json') as json_file:
        selected_edition = json.load(json_file)
        data = [item for item in selected_edition['collection_items'] if
                item["source"] == source and item["category"] == category][0]
        url = 'https://gpz5a716s2.execute-api.eu-west-1.amazonaws.com/dev/test_collection'

        output = requests.post(url, json=data)

        display_handler.print_in_file(output)

        items = output.json()["sucess"]["result"]
        display_handler.print_html_in_browser(items)


if __name__ == "__main__":
    test_collection("GB", "dailyrecord.co.uk", "category_local_news")
